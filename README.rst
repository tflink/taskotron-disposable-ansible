taskotron-disposable-ansible
============================

This is a set of ansible playbooks and roles that I threw together for poking
at a Taskotron deployment with disposable clients.

It's mostly copied from `taskotron-ansible <https://bitbucket.org/fedoraqa/taskotron-ansible>`_
and you probably shouldn't be using this repo unless you know what you're doing.
It does not work out of the box and will need significant tweaking before it
does work on other systems.

The private files structure should pretty much be the same as taskotron-ansible
but that is a repo which is not publicly accessible.
